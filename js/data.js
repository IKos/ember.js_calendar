Calendar.Day = Ember.Object.extend({
  day:   null,
  month: null,
  year:  null,

  fullDate: function() {
  	var day = this.get('day');
  	if (day < 10){
  		day = '0' + day;
  	};
  	var month = this.get('month');
  	if (month < 10){
  		month = '0' + month;
  	};
  	var year = this.get('year');
  	return (day + '.' + month + '.' + year);
  }.property('day', 'month', 'year')
});

var	today = Calendar.Day.create({
      day : new Date().getDate(),
      month : new Date().getMonth() + 1,
      year : new Date().getFullYear()
});

var selectedDay = Calendar.Day.create({
      day : new Date().getDate(),
      month : new Date().getMonth() + 1,
      year : new Date().getFullYear()
});

Calendar.IndexOfDay = Ember.Object.extend({
	index : null,
	checktoday : null,
	checkselday : null,
	thismonth : null
})

Calendar.Week = Ember.Object.extend({
	index: null,
	dayIndex: null
});

Calendar.Month = Ember.Object.extend({
  index:    null,
  name:     null,
  countDay: null
});

var month = [
 {
   index: 1,
   name: 'January',
 },
 {
   index: 2,
   name: 'February',
 },
 {
   index: 3,
   name: 'March',
 },
 {
   index: 4,
   name: 'April',
 },
 {
   index: 5,
   name: 'May',
 }, 
 {
   index: 6,
   name: 'June',
 },
 {
   index: 7,
   name: 'July',
 }, 
 {
   index: 8,
   name: 'August',
 },
 {
   index: 9,
   name: 'September',
 }, 
 {
   index: 10,
   name: 'October',
 },
 {
   index: 11,
   name: 'November',
 },
 {
   index: 12,
   name: 'December',
 }
];