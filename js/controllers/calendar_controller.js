Calendar.DateController = Ember.ObjectController.extend({

  goRoute: function (needDay) {
		this.transitionToRoute('date', {year: needDay.year, month: needDay.month, day: needDay.day});
	},

	actions: {

	    goToday: function () {
	    	selectedDay.year = today.year;
	    	selectedDay.month = today.month;
	    	selectedDay.day = today.day;
	    	this.goRoute(today);
	    },

      incYear: function() {
      	selectedDay.year = this.get('model.year');
	      ++selectedDay.year;
	      this.goRoute(selectedDay);
	    },

	    decYear: function() {
	    	selectedDay.year = this.get('model.year');
	      --selectedDay.year;
	      this.goRoute(selectedDay);
	    },

	    incMonth: function() {
	    	selectedDay.month = this.get('model.month');
	      if (selectedDay.month == 12)
	      {
	      	++selectedDay.year;
	      	selectedDay.month = 1;
	      }
	      else
	        ++selectedDay.month;

	      this.goRoute(selectedDay);
	    },

	    decMonth: function() {
	    	selectedDay.month = this.get('model.month');
	      if (selectedDay.month == 1)
	      {
	      	--selectedDay.year;
	      	selectedDay.month = 12;
	      }
	      else
	        --selectedDay.month;

	      this.goRoute(selectedDay);
	    },

      goSelectedDate: function(day) {
      	selectedDay.day = day;
      	this.goRoute(selectedDay);
      }

	},

  fillTable: function() {
  	console.log('fillTable called', this.get('model'));

	  var firstDayMonth = new Date(this.get('model.year'), this.get('model.month') - 1, 1).getDay();
	  var countDayPrevMonth = new Date(this.get('model.year'), this.get('model.month') - 1, 0).getDate();
	  var countDayMonth = new Date(this.get('model.year'), this.get('model.month'), 0).getDate();

	  var firstWeekMonth = getWeek(this.get('model.year'), this.get('model.month') - 1, 1);

	  var week = [];
	  var currentDay = 1;
	  var realyThisMonth = true;

	  for (var i = 1; i < 7; ++i){
	  	var day = [];

		  for (var j = 1; j < 8; ++j){
		  	if (currentDay > countDayMonth) 
		  	{
		  		currentDay = 1;
		  		realyThisMonth = !realyThisMonth;
		  	}
		  	if (i == 1 && j < firstDayMonth)
		  	{
		  		day[j-1] = Ember.Object.create({
		  			index : countDayPrevMonth - firstDayMonth + (j+1),
		  			checktoday : false,
		  			checkselday : false,
		  			thismonth : !realyThisMonth
	  			});
		  	}
		  	else
		  	{
		  		day[j-1] = Ember.Object.create({
		  			index : currentDay,
		  			checktoday : (realyThisMonth && this.get('model.year') == today.year && this.get('model.month') == today.month && currentDay==today.day)?true:false,
		  			checkselday : realyThisMonth && currentDay==this.get('model.day')?true:false,
		  			thismonth : realyThisMonth
	  			});
		  		++currentDay;
		  	}		  	
		  }

		  if (day.compact().length < 1)
		  {
		  	break;
		  }
		  week[i-1] = Ember.Object.create({
		  	index: firstWeekMonth++, 
	  		dayIndex: day });
	  }
	  return week;
  }.property('model.year', 'model.month', 'model.day'),

  getCurrentDate: function () {
    return today.get('fullDate');
  }.property('date'),

  getSelectedDay_year: function () {
		return this.get('model.year');
	}.property('date'),

  getSelectedDay_month: function () {
  	return month.findBy('index', Number(this.get('model.month'))).name;
	}.property('date')

});

getWeek = function (year, month, day) {
    var target  = new Date(year, month, day);
    var dayNr   = (target.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    var firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() != 4) {
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
    }
    return 1 + Math.ceil((firstThursday - target) / 604800000);
};