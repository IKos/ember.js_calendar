Calendar.Date = DS.Model.extend({
   year  : DS.attr('int'),
   month : DS.attr('int'),
   day   : DS.attr('int')
});